import fs from 'node:fs';
import os from 'node:os';
import path from 'node:path';
import archiver from 'archiver';
import { nanoid } from 'nanoid';
import ignore from 'ignore';
import fnetListFiles from '@fnet/list-files';

/**
 * Zips files in the source directory that match the provided glob patterns, excluding the patterns specified in .gitignore file if present.
 * Maintains the directory structure as in the source directory and supports recursion.
 * Extras parameter allows adding additional files or directories outside of the source directory.
 * Ensures that each file or directory is only added once even if matched by multiple patterns.
 * @param {Object} args - The object containing function parameters.
 * @param {string} args.sourceDir - The path of the source directory where the files will be zipped.
 * @param {string[]} args.patterns - The list of glob patterns to match in the source directory.
 * @param {string} [args.outputDir] - The directory where the generated zip file will be saved. If not specified, the operating system's temporary directory is used.
 * @param {boolean} [args.useGitIgnore=true] - Whether to use .gitignore file to exclude files from being zipped.
 * @returns {Promise<Object>} Returns an object containing the full path of the generated zip file, and lists of the added files and folders.
 */
export default async ({ sourceDir, pattern, outputDir, useGitIgnore = false }) => {
  const tempDir = outputDir || os.tmpdir();
  const zipFileName = `${nanoid()}.zip`;
  const zipFilePath = path.join(tempDir, zipFileName);
  pattern = Array.isArray(pattern) ? pattern : pattern ? [pattern] : [];
  // extras = Array.isArray(extras) ? extras : extras ? [extras] : [];

  const ig = ignore();
  if (useGitIgnore) {
    try {
      const gitignore = await fs.promises.readFile(path.join(sourceDir, '.gitignore'), 'utf8');
      ig.add(gitignore.split(/\r?\n/));
    } catch (error) {
      console.error('Error reading .gitignore file:', error);
    }
  }

  const archive = archiver('zip', { zlib: { level: 9 } });
  const output = fs.createWriteStream(zipFilePath);

  archive.pipe(output);

  const filesList = [];
  const foldersList = [];

  const files = await fnetListFiles({ pattern, dir: sourceDir, nodir: false, absolute: true });
  for (const file of files) {
    const filePath = file;
    const relativePath = path.relative(sourceDir, filePath);
    if (useGitIgnore && ig.ignores(filePath)) {
      continue;
    }
    const stats = await fs.promises.lstat(filePath);
    if (stats.isDirectory()) {
      foldersList.push(relativePath);
      archive.directory(filePath, relativePath);
    } else {
      filesList.push(relativePath);
      archive.file(filePath, { name: relativePath });
    }
  }

  await archive.finalize();

  await new Promise((resolve, reject) => {
    output.on('close', () => resolve());
    output.on('error', reject);
    archive.on('error', reject);
  });

  return {
    path: zipFilePath,
    files: filesList,
    folders: foldersList
  };
};