# @fnet/dir-zipper

The @fnet/dir-zipper project is a Node.js application that acts as a file and directory zipping utility. 

It allows users to compress a set of files and directories within a source directory. This set can be defined through glob patterns provided by the user. The resulting compressed file or 'zip' will preserve the original directory structure of the source directory, allowing for easy decompression and organization later.

## Key Functionalities

The utility is designed to match input files and directories based on user-specified glob patterns, and then compress them into a single zip file. It allows to exclude specific files and directories from being zipped by parsing the .gitignore file if it's present in the source directory. This makes it a handy tool for excluding unnecessary files (for instance, log files, cache directory, etc.) during the compression process.

During the zipping operation, the algorithm ensures each file or directory is only added once, even it is matched by multiple glob patterns.

The package, by default, places the resultant zip file into the system's temporary directory. However, it also gives an option to select a preferred output directory.

## Return Value

Upon completion, the program returns a Promise with an object. This object contains the full path of the newly created zip file along with lists of the added files and folders. This information can be useful for log keeping and verification of the zipping process.

**Note**: The application emphasizes efficient space usage by zipping with maximum (level 9) zlib compression level. This optimization provides significant savings in file size, ensuring the resultant zip file is as small as possible. 

## Exception Handling

In addition to the core functionality, the given project also includes basic exception handling to manage errors relating to reading the .gitignore file, closing the output stream, and finalizing the archiver.

## Conclusion

In summary, @fnet/dir-zipper is a useful file and directory zipping utility that allows the user to select specific files or directories via glob patterns, ignore files via .gitignore, and store the resultant zip file in a location of their choosing. It makes archiving and transferring files organized, efficient, and user-friendly.